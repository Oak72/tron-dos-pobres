package com.company;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private ServerSocket serverSocket;
    private ClientConnection[] connections = new ClientConnection[4];
    private ExecutorService executorService= Executors.newCachedThreadPool();


    public Server() throws IOException {
        this.serverSocket =  new ServerSocket(666);

    }

    public void init() throws IOException, InterruptedException {
        for (int i = 0; i < 4; i++) {
            Socket s = serverSocket.accept();
            ClientConnection client = new ClientConnection(s,ReadASCII.read("/tron.txt",Color.BLACK,Color.CYAN));
            connections[i]=client;
        }
        Game game= new Game(connections);
        for (ClientConnection connection : connections) {
            executorService.submit(connection);
        }
        System.out.println("[Server] Game Start!");
        game.start();
        }




}


