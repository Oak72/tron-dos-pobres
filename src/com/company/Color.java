package com.company;


public enum Color {
    BLACK(0),
    RED(1),
    GREEN(2),
    YELLOW(3),
    BLUE(4),
    PINK(5),
    CYAN(6),
    WHITE(7),
    ;

    Integer code;
    private static final String PREFIX = "\u001B[";
    private static final String SUFFIX = "m";
    private static final String RESET = "\u001B[0m";
    private static final Color DEFAULT = BLACK;
    public static final int verticalModifier = 10;
    public static final int corneringModifier = 100;
    Color(Integer code){
        this.code = code;
    }

    //

    public static Integer getIndex(Color color, boolean isVertical,boolean isCornering) {
        int code = 0;
        switch (color){
            case RED:
                code = 1;
                break;
            case BLUE:
                code = 2;
                break;
            case GREEN:
                code = 3;
                break;
            case YELLOW:
                code = 4;
                break;
            case BLACK:
                code = BLACK.code;
                break;
            default:
                return -1;
        }

        if(isVertical){
            code +=verticalModifier;
        }
        if(isCornering){
            code +=corneringModifier;
        }

        return code;
    }

    public static Color getColorFromIndex(Integer index){
        if(index<=0) return DEFAULT;
        if(index>=corneringModifier) index -=corneringModifier;
        if(index>=verticalModifier) index -=verticalModifier;
        Color[] colors = {RED,BLUE,GREEN,YELLOW};
        return colors[index-1];
    }

    public String parseWithColour(String message){
        return PREFIX + this.code + SUFFIX +  message + RESET;
    }

    public static Color getDefault() {
        return DEFAULT;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public String getColorAsString(){
        return this.code.toString();
    }
}
