package com.company;

import java.util.Iterator;

public class Grid{
    //public static final int MAX_X=20, MAX_Y=10;
    public static final int MAX_X=40, MAX_Y=30;

    private Integer[][] map;


    public Grid() {
        createMap();
    }

    private void createMap(){
        map= new Integer[MAX_X][MAX_Y];
        for (int i = 0; i <map.length ; i++) {
            for (int j = 0; j < map[0].length; j++) {
                setColor(i,j,Color.BLACK,false,false);
            }

        }
    }

    public void setColor(int x, int y, Color color,boolean isVertical,boolean isCornering){
        map[x][y] = Color.getIndex(color,isVertical,isCornering);

    }


    public Integer getIntegerAt(int x, int y){
        return map[x][y];
    }

    public void deleteTrail(Player player){
        for (int i = 0; i < MAX_X; i++) {
            for (int j = 0; j < MAX_Y; j++) {
                if(map[i][j].equals(Color.getIndex(player.getColor(),false,false))){
                    setColor(i,j,Color.BLACK,false,false);
                }
                if(map[i][j].equals(Color.getIndex(player.getColor(),true,false))){
                    setColor(i,j,Color.BLACK,false,false);
                }
                if(map[i][j].equals(Color.getIndex(player.getColor(),true,true))){
                    setColor(i,j,Color.BLACK,false,false);
                }if(map[i][j].equals(Color.getIndex(player.getColor(),false,true))){
                    setColor(i,j,Color.BLACK,false,false);
                }

            }

        }
    }


}

