package com.company;

public enum Directions {
    LEFT("A",-1,0),
    RIGHT("D",1,0),
    UP("W",0,-1),
    DOWN("S",0,1);

    private int moveX, moveY;
    private String key;

    Directions(String key,int moveX, int moveY) {
        this.key = key;
        this.moveX = moveX;
        this.moveY = moveY;
    }

    public Directions getOpposite(){
        switch (this){
            case LEFT:
                return RIGHT;
            case RIGHT:
                return LEFT;
            case UP:
                return DOWN;
            default:
                return UP;
        }
    }

    public static Directions getDirection(String direction)throws IllegalArgumentException{
        System.out.println("Entered getDirection()");
        for (Directions value : Directions.values()) {
            System.out.println("getDirection has " + direction);
            if(direction.equalsIgnoreCase(value.getKey())) return value;
        }
     throw new IllegalArgumentException("Invalid direction");
    }

    public int getMoveX() {
        return moveX;
    }

    public int getMoveY() {
        return moveY;
    }


    public boolean isVertical(){
        return !this.equals(RIGHT) && !this.equals(LEFT);
    }

    public String getKey() {
        return key;
    }
}
