package com.company;

public class Player implements Comparable<Player> {
      private int x,y;
      private Directions directions;
      private Directions lastDirection;
      private Color color;
      private Grid grid;
      private boolean isDead;
      private int movementCount = 0;
      private boolean isCornering;
      private String representation = " P "; //By default

   public Player(int x, int y, Directions directions,Color color,Grid grid) {
        this.x = x;
        this.y = y;
        this.directions = directions;
        this.lastDirection = directions;
        this.color = color;
        this.grid=grid;
    }

    public Player(int x, int y, Directions directions,Color color,Grid grid, String representation) {
        this.x = x;
        this.y = y;
        this.directions = directions;
        this.lastDirection = directions;
        this.color = color;
        this.grid=grid;
        this.representation = representation;
    }

    public boolean move(){
       if(isDead) return true;//sergio move

        movementCount++;
        changeColorMap();
        if (checkBorders()) {
            x += directions.getMoveX();
            y += directions.getMoveY();
        }
        if(stayingAlive(x,y)){
            lastDirection=directions;
            return true;
        }
        isDead = true;
        return false;

    }
    private boolean checkBorders(){
       if ((x+directions.getMoveX())==-1){
           x=Grid.MAX_X-1;
           return false;
       }
       if ((x+directions.getMoveX())==Grid.MAX_X){
           x=0;
            return false;
        }
       if ((y+directions.getMoveY())==-1){
           y=Grid.MAX_Y-1;
           return false;
       } if ((y+directions.getMoveY())==Grid.MAX_Y){
           y = 0;
           return false;
        }
            return true;
    }
    private boolean stayingAlive(int x, int y){
       return grid.getIntegerAt(x, y).equals(Color.getIndex(Color.BLACK,false,false));
    }

    public void changeColorMap(){
         grid.setColor(x,y,color,directions.isVertical(),isCornering);
         isCornering =false;
    }

    public void setDirections(Directions directions){

         if (directions.equals(lastDirection.getOpposite())|| directions.equals(this.directions)){
               return;
         }
         this.directions=directions;
         isCornering = true;
    }

    public Color getColor() {
        return color;
    }

    public boolean isAt(int x, int y) {
       return this.x == x && this.y == y;
    }

    public String getRepresentation() {
       return representation;
    }

    @Override
    public String toString() {
        return this.getColor().toString();
    }

    public boolean isDead() {
       return isDead;
    }

    public void kill() {
       this.isDead = true;
    }

    public int getMovementCount() {
        return movementCount;
    }

    @Override
    public int compareTo(Player o) {
       if(this.isDead() && o.isDead()){
           System.out.println(o +  " comparing to " + this );
           System.out.println(o.getMovementCount() +  " comparing to " + this.getMovementCount());
           return  o.getMovementCount() - this.getMovementCount();
       }
      return (isDead()) ? 1 : -1 ;
    }


    public int getX() {
       return x;
    }

    public int getY() {
        return y;
    }
}
