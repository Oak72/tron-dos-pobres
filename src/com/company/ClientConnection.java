package com.company;

import java.io.*;
import java.net.Socket;
import java.sql.SQLOutput;

public class ClientConnection implements Runnable{
    private Socket socket;

    private Player player;

    PrintWriter writer;
    BufferedReader reader;


    public ClientConnection(Socket socket,String initialMessage) {
        this.socket= socket;
        try {
            this.writer = new PrintWriter(socket.getOutputStream(),true);
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sendMessage(initialMessage);
        sendMessage("Connection Established");


    }

    @Override
    public void run() {
        sendMessage(ReadASCII.read("/instructions.txt"));


        while (true){
            //System.out.println("Read Player Input of Player " + player.getColor());
            readPlayerInput();

        }
    }





    private void readPlayerInput(){
        try {
            System.out.println("Read Player Input");
            System.out.println("Player is " + player.toString());
            System.out.println("Reading Player Input for " + player.getColor().toString());
            player.setDirections(Directions.getDirection(reader.readLine()));
            System.out.println("Try Successful on readPlayerInput()");
        } catch(IllegalArgumentException e){
            //Invalid input
            System.err.println("Illegal Argument Exception when readPlayerInput() " + e.getMessage());
            return;
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message){
        writer.println(message);
    }


    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }


}
