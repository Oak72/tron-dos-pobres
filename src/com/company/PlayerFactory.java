package com.company;

import javax.print.DocFlavor;

public class PlayerFactory {

    public static Player[] playerCreation(Grid grid){

        Player[]players= new Player[4];
        players[0]= new Player(0,0,Directions.RIGHT,Color.RED,grid," ☭ ");
        players[1]= new Player(Grid.MAX_X-1,0,Directions.DOWN,Color.BLUE,grid," ⚔︎ ");
        players[2]= new Player(Grid.MAX_X-1,Grid.MAX_Y-1,Directions.LEFT,Color.GREEN,grid," ☪︎ ");
        players[3]= new Player(0,Grid.MAX_Y-1,Directions.UP,Color.YELLOW,grid," ☺︎ "); //✜ //☺
        return players;
       }
    }

