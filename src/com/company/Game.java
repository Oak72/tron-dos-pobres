package com.company;

import java.util.Arrays;
import java.util.PriorityQueue;

public class Game {
    public static final String BLANK = "_"; //TODO change this
    ClientConnection[]connections;
    Player[]players;
    Grid grid;
    private int deadCount=0;

    public Game(ClientConnection[]clientConnections) {
        connections=clientConnections;
        grid= new Grid();
        players=PlayerFactory.playerCreation(grid);
        playerToClient();
    }
    private void playerToClient(){
        for (int i = 0; i < connections.length ; i++) {
            connections[i].setPlayer(players[i]);
        }
    }

    public void start() throws InterruptedException {
        System.out.println("[Game]: Game is starting !");

        try {
            Thread.sleep(5000);
            sendEach("You are Player ");
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (!isGameOver()){
            System.out.println("[Game]: while cycle executing.");
            playerMove();
            sendAll(buildOutputString());
            Thread.sleep(600);
        }
        gameOver();
    }

    private void sendEach(String message) {
        for (ClientConnection clientConnection : connections) {
            clientConnection.sendMessage(FontWrapper.wrap(
                    message + clientConnection.getPlayer(),Color.BLACK,clientConnection.getPlayer().getColor()));
        }
    }

    private String buildOutputString(){
        StringBuilder builder = new StringBuilder();
        FontWrapper font = new FontWrapper();

//
        String str = "\u007C";
        final String corner = " ✜ ";
        final String vertical = " 🁢 ";
        final String horizontal = "🀰 🀰";
        final String try1 = "   ";
        String here= "";


        for (int y = -1; y <= Grid.MAX_Y; y++) {
            for (int x = -1; x <= Grid.MAX_X; x++) {
                if(y == -1 || y == Grid.MAX_Y){
                    builder.append("- -");
                    continue;
                }
                if(x ==-1 || x == Grid.MAX_X){
                    builder.append(" | ");
                    continue;
                }
                font.setForeground(Color.getColorFromIndex(grid.getIntegerAt(x,y))).setBackground(Color.BLACK);

                if(grid.getIntegerAt(x,y).equals(Color.BLACK.code)){
                    here = try1;
                }else{
                    here = font.wrap((grid.getIntegerAt(x, y) >= Color.verticalModifier) ? vertical : horizontal);
                }
                if(grid.getIntegerAt(x, y) >= Color.corneringModifier) {
                    here = font.wrap(corner);
                }

                for (int i = 0; i < players.length; i++) {
                    if(players[i].isAt(x,y)){
                        font.setForeground(players[i].getColor());
                        here = font.wrap(players[i].getRepresentation());
                        break;
                    }
                }

                builder.append(here);
            }
            builder.append("\n");
        }

        return builder.toString();
    }

    private void playerMove(){
        for (Player player : players) {
            if(player.move()){
                continue;
            }
            playerCollision(player);
            deletePlayer(player);

        }
    }
    private void playerCollision(Player current){
        if(current.isDead())return;
        for (Player player : players) {
            if (player.equals(current)){
                continue;
            }
           if (player.isAt(current.getX(),current.getY())){
                player.kill();
                current.kill();
            }


        }
    }
    private void deletePlayer(Player player){
        grid.deleteTrail(player);
        player.kill();
        deadCount++;
    }

    public void sendAll(String message){
        for (ClientConnection connection : connections) {
            if (connection.getPlayer().isDead() && !isGameOver()){
                connection.sendMessage(ReadASCII.read("/xd.txt"));
                continue;
        }
            connection.sendMessage(message);

        }
    }

    private boolean isGameOver() {
        return !(deadCount<3);
    }


    private void gameOver(){
        StringBuilder builder = new StringBuilder();

        PriorityQueue<Player> queue = new PriorityQueue<>();

        System.out.println();

        for (Player player : players) {
            queue.add(player);
        }



        int counter = 2;

        builder.append(String.format("#1: Player %s has survived TRON",FontWrapper.wrap(queue.peek().toString(),Color.BLACK,queue.poll().getColor())));
        while (!queue.isEmpty()){
            builder.append("\n");
            String player = FontWrapper.wrap(queue.peek().toString(),Color.BLACK,queue.peek().getColor());
            builder.append(String.format("#%d: Player %s has moved %d times",counter++,player,queue.poll().getMovementCount()));
        }
      sendAll(builder.toString());
    }
}
