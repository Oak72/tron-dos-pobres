package com.company;

import java.io.*;

public class ReadASCII {


    public static String read(String path){
        StringBuilder builder = new StringBuilder();

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(getFileInputStream(path)));

            String line =  "";
            while ((line=reader.readLine()) != null){
                builder.append(line+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return builder.toString();
    }


    public static String read(String path, Color background, Color foreground){
        FontWrapper wrapper = new FontWrapper();
        wrapper.setBackground(background);
        wrapper.setForeground(foreground);
        return wrapper.wrap(read(path));
    }

    public static InputStream getFileInputStream(String path){
        return ReadASCII.class.getResourceAsStream(path);
    }
}
