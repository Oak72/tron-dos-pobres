package com.company;

import java.awt.*;

public class FontWrapper {

    private boolean bold;
    private String background;
    private String foreground;

    private static final String prefix = "\u001b[";
    private static final String reset = "\u001b[0m";


/*    public static void main(String[] args) {
        FontWrapper wrapper = new FontWrapper();
        wrapper.setBackground(Colour.RED);
        wrapper.setForeground(Colour.BLUE);
        System.out.println(wrapper.wrap("girly boobies"));
    } */


    public FontWrapper(){
        setBackground(Color.BLACK);
        setForeground(Color.WHITE);
    }



    public FontWrapper setBackground(Color color){
        final String suffix = "m";

        background = String.format("%s4%s%s",prefix,color.getColorAsString(),suffix);

        return this;

    }

    public FontWrapper setForeground(Color color){
        final String suffix = ";1m";
        foreground = String.format("%s3%s%s",prefix,color.getColorAsString(),suffix);
        return this;
    }


    public String wrap(String message) {
       return String.format("%s%s%s%s", background,foreground,message,reset);
    }

    public void setForeground(String colorFromCode) {
        final String suffix = "m";

        background = String.format("%s4%s%s",prefix,colorFromCode,suffix);
    }

    public static String wrap(String message, Color background, Color foreground) {
        FontWrapper wrapper = new FontWrapper();
        wrapper.setBackground(background);
        wrapper.setForeground(foreground);
        return wrapper.wrap(message);
    }
}
